import speech_recognition as sr
import subprocess
import os


def executeJava():
    s = subprocess.check_output("javac TestFile.java", shell=True)
    p = subprocess.check_output("java TestFile", shell=True)
    print(p.decode("utf-8"))


def deleteContent(pfile):
    pfile.seek(0)
    pfile.truncate()


def commands_to_code(argument, file):
    switcher = {
        "open class": "\nclass TestFile\n{",
        "open head": "\npublic static void main(String args[])\n{",
        "print hello": "\nSystem.out.println(\"hello\");",
        "close the bracket": "\n}",
        "import input output":"\nimport java.io.*;",
        "import utility":"\nimport java.util.*;",
        "exit": "exited successfully"
    }
    if argument in switcher:
        file.write(switcher[argument])
        print(argument + " registered successfully")
    elif "initialise integer" in argument and len(argument.split())==5:
        l = argument.split()
        file.write("\nint "+l[2].lower()+"="+l[4]+";")
        print(argument + " registered successfully")
    elif "print addition" in argument and len(argument.split())==6:
        l = argument.split()
        file.write("\nSystem.out.println("+l[3].lower()+"+"+l[5].lower()+");")
        print(argument + " registered successfully")
    elif "print subtraction" in argument and len(argument.split())==6:
        l = argument.split()
        file.write("\nSystem.out.println("+l[3].lower()+"-"+l[5].lower()+");")
        print(argument + " registered successfully")
    elif "print division" in argument and len(argument.split())==6:
        l = argument.split()
        file.write("\nSystem.out.println("+l[3].lower()+"/"+l[5].lower()+");")
        print(argument + " registered successfully")
    elif "print multiplication" in argument and len(argument.split())==6:
        l = argument.split()
        file.write("\nSystem.out.println("+l[3].lower()+"*"+l[5].lower()+");")
        print(argument + " registered successfully")
    elif "print" in argument and len(argument.split())==2:
        l = argument.split()
        file.write("\nSystem.out.println("+l[1].lower()+");")
        print(argument + " registered successfully")
    else:
        print("you said \"" + argument + "\" which is not in grammar")


if __name__ == "__main__":
    r = sr.Recognizer()
    f = open("TestFile.java", "a+")
    text = ""
    i = 1
    while i == 1:
        with sr.Microphone() as source:
            r.adjust_for_ambient_noise(source)
            #r.dynamic_energy_threshold = True
            print('Speak something: ')
            #audio = r.record(source, duration = 5, offset = None)
            audio = r.listen(source)
            try:
                text = r.recognize_google(audio)
            except:
                print("something went wrong")
                continue
        if text == "exit":
            print("exiting loop")
            break
        else:
            commands_to_code(text, f)
    print("Here's your output", end=":")
    executeJava()
    f.close()
    os.remove("TestFile.java")
    os.remove("TestFile.class")